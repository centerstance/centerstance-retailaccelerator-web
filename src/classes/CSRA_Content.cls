global with sharing class CSRA_Content {
	
	public Id 		id 				{get; set;}
	public String 	title 			{get; set;}
	public String	blurb			{get; set;}
	public Id   	imageId			{get; set;}
	public String   imageBinaryData	{get; set;}
	public String	imageURL 		{get; set;}

}