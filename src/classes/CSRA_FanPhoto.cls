public without sharing class CSRA_FanPhoto {
        // This class holds data about a Fan photo, they are saved on Base64 encoded format.
        public Id       photoId { get; set; }        
        public String   photoBinaryData { get; set; }
        public String   photoName { get; set; }         
        public String   contentType { get; set; }   

}