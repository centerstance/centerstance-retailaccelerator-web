global with sharing class CSRA_EarningTransaction {
	
	public String    id 					{get; set;}	
	public String    contactId 				{get; set;}
	public String    name					{get; set;}
	public Decimal   transactionPoints	 	{get; set;}
	public DateTime  transactionDateTime 	{get; set;}
	public Date  	 effectiveDate			{get; set;}
	public Date  	 expirationDate			{get; set;}
			

}