public without sharing class CSRA_FanServiceHelper {
    
    //custom exception
    public class FanServiceHelperException extends CS_CustomException {} 
    
    //logging level for the class
    private System.LoggingLevel debugLevel = LoggingLevel.INFO;
    
    private Contact fanContact;  
    
    // Given the Current User's context, get the Community user's contact ID. 
    // Made public because CSRA_EarningTransactionServiceHelper needs to obtain the contactId
    
    public Id getContactId()
    {
		Id contactId;
		
		List<User> users = [Select Id, ContactId From User where Id = : UserInfo.getUserId() Limit 1];
		
		if(!users.IsEmpty())
		{
			contactId = users.get(0).ContactId;
		} 
		else 
		{
			throw new FanServiceHelperException('Unable to find contact');
		}
        
		return contactId;	
    }  
            
    public CSRA_Fan getFan ()
 	{        
        Id ContactId = getContactId();
        
        // Read Fan profile data, Earning Transactions, Reward Transactions. 
        
        List<Contact> contacts = [
        							Select 	Id, 
        									FirstName, 
        									LastName, 
        									Email, 
        									Title, 
        									Phone, 
        									MailingStreet, 
        									MailingCity, 
        									MailingState, 
        									MailingPostalCode, 
        									MailingCountry, 
        									Gender__c, 
        									BirthDate, 
        									TotalPoints__c, 
        									RemainingPoints__c, 
        									PointsUntilNextTier__c, 
        									PhotoUploaded__c, 
        									ProfileCompleted__c, 
        									RewardTier__r.Id, 
        									RewardTier__r.Order__c, 
        									RewardTier__r.AchievementMessage__c, 
        									RewardTier__r.AchievementName__c, 
        									RewardTier__r.ThresholdPoints__c, 
        									RewardTier__r.Name, 
        										(	
        											Select 
        												Id, 
        												Name, 
        												EffectiveDate__c, 
        												ExpirationDate__c, 
        												EarnedPoints__c, 
        												EarnedDateTime__c 
        											From ContactEarningTransactions__r
        											order by EarnedDateTime__c desc
        										),  
        										(
        											Select 
        												Name, 
        												RewardItem__c, 
        												RedeemedPoints__c, 
        												RedeemedDateTime__c 
        											From ContactRewardTransactions__r
        											order by RedeemedDateTime__c desc
        										) 
        										
        										From Contact where Id = :ContactId
        							];
         
        CSRA_Fan fan = new CSRA_Fan();        
            
        if(!contacts.IsEmpty()){

        	fanContact = contacts.get(0);						// having to keep the fanContact in global scope because, I cant get a handle of the contact without another query
                    
	        fan.contactId                   = fanContact.Id;
	        fan.FirstName                   = fanContact.FirstName;
	        fan.LastName                    = fanContact.LastName;	        
	        fan.Email                       = fanContact.Email;
	        fan.Title                       = fanContact.Title;          
	        fan.Phone                       = fanContact.Phone; 
	        fan.mailingStreet               = fanContact.mailingStreet; 
	        fan.mailingState                = fanContact.mailingState;          
	        fan.mailingPostalCode           = fanContact.mailingPostalCode;          
	        fan.mailingCountry              = fanContact.mailingCountry;          
	        fan.mailingCity                 = fanContact.mailingCity;          
	        fan.gender                      = fanContact.gender__c;          
	        fan.email                       = fanContact.email;          
	        
	        if (fanContact.TotalPoints__c == null)
	        	fan.totalPoints  = 0;
			else
				fan.totalPoints             = fanContact.TotalPoints__c; 
	        	
	        if (fanContact.RemainingPoints__c == null)
	        	fan.redeemablePoints = 0;
	        else
	        	fan.redeemablePoints             = fanContact.RemainingPoints__c;
	        	
	        if (fanContact.PointsUntilNextTier__c == null)
	        	fan.pointsUntilNextTier = 0;
	        else
	        	fan.pointsUntilNextTier        	= fanContact.PointsUntilNextTier__c.intValue(); 		        
	        
	        fan.birthDate                   = fanContact.birthDate;  
	        
	        fan.isPhotoUpdated              = 'false';  	// always false on the GET, no persistant attribute	 
	               
	        CSRA_RewardTier rewardTier = new CSRA_RewardTier();
	        
	        if (fanContact.RewardTier__r.Id != null)
	        {
		        rewardTier.id = fanContact.RewardTier__r.Id;
		        rewardTier.name = fanContact.RewardTier__r.Name;
		        rewardTier.achievementName = fanContact.RewardTier__r.AchievementName__c;
		        rewardTier.achievementMessage = fanContact.RewardTier__r.AchievementMessage__c;
		        rewardTier.thresholdPoints = fanContact.RewardTier__r.ThresholdPoints__c.intValue();
		        rewardTier.order = fanContact.RewardTier__r.Order__c.intValue();
	        	        	        	        
	        	fan.rewardTier = rewardTier;		        	
	        }
	         
	        List<CSRA_EarningTransaction> listEarningTransactions = new  List<CSRA_EarningTransaction>();
	        	                          	
	        for ( ContactEarningTransaction__c Cet : fanContact.ContactEarningTransactions__r ){
	            		
	        	CSRA_EarningTransaction earningTran = new CSRA_EarningTransaction();
	            		
	           	earningTran.id = Cet.id;	
	           	earningTran.name = Cet.Name;
	           	
	           	if ( Cet.EarnedPoints__c != null)	            		
	           		earningTran.transactionpoints = Cet.EarnedPoints__c;
	           		
	           	earningTran.transactionDateTime 	= Cet.EarnedDateTime__c;	           	
	           	earningTran.expirationDate 			= Cet.ExpirationDate__c;
	           	earningTran.effectiveDate 			= Cet.EffectiveDate__c;
	           	earningTran.contactId 				= fanContact.Id;

	           	listEarningTransactions.add(earningTran);
	            		
	           	system.debug ( 'ContactEarningTransaction__c  Name ' + Cet.Name );	            		
	       	}
	        
	        fan.earningTransactions = listEarningTransactions;
	        
	        List<CSRA_RewardTransaction> listRewardTransactions = new  List<CSRA_RewardTransaction>();
	                     	
	        for ( ContactRewardTransaction__c Crt : fanContact.ContactRewardTransactions__r ){
	            		
	        	CSRA_RewardTransaction rewardTran = new CSRA_RewardTransaction();
	            		
	           	rewardTran.id = Crt.id;
	           	rewardTran.rewardTransactionName = Crt.Name;	            		
	           	rewardTran.redeemedDateTime = Crt.RedeemedDateTime__c;
	           	
	           	if ( Crt.RedeemedPoints__c != null)
	           		rewardTran.redeemedPoints = Crt.RedeemedPoints__c.format() ;           	
	           	
	           	listRewardTransactions.add(rewardTran);
	            		
	           	system.debug ( 'ContactRewardTransaction__c Name ' + Crt.Name );	            		
	       	}
	        
	        fan.rewardTransactions = listRewardTransactions;	        
	        
	        CSRA_FanPhoto fanPhoto = new CSRA_FanPhoto();

			List<Attachment> attachments = [ Select Id, IsDeleted, ParentId, Name, BodyLength, Body, Description, contentType From Attachment Where ParentId = :ContactId Order by CreatedDate Desc limit 1];
			
			System.Debug( 'Attachments.size ' + attachments.size() );  		
  			
  			if(!attachments.IsEmpty()){
  			
  				fanPhoto.photoName = attachments.get(0).Name;
  				
  				fanPhoto.photoBinaryData = EncodingUtil.base64Encode(attachments.get(0).Body);
  				
  				fanPhoto.photoName = attachments.get(0).Name;
  				
  				fanPhoto.photoId = attachments.get(0).Id;
  				
  				fanPhoto.ContentType = attachments.get(0).contentType;	

				fan.fanPhoto = fanPhoto;
  			}
  			else
  			{
				System.Debug( 'No fan photo, ContactId ' + ContactId);		  			
  			}  		
        }
        else 
        {
            throw new FanServiceHelperException('Unable to get fan information, ContactId ' + ContactId);
        }
                
        return fan;
 	}
     
    //Method to edit fan personal data (like name, mailing address etc.) also Fan Photo
    
    public CSRA_Fan updateFan(CSRA_Fan fan)
    {    
        CSRA_Fan fanToUpdate;
              
        fanToUpdate = getFan( );
        
        system.debug( ' Fan contact at start of updateFan ' + fanContact);
        
        if (fan.isPhotoUpdated.toLowerCase() == 'true')
        {        		       
			// Update the Photo //////////////////////////////////////////////////////////////////  
		    
			if ( fan.fanPhoto != null )	// Is there JSON attribute for Photo?
			{
				String imageBody = fan.fanPhoto.photoBinaryData;	
		
				system.debug( 'Image Body ' + imageBody);
						
				Blob encodedImage = EncodingUtil.base64Decode(imageBody);	//Decode the base64 encoded image ..
			  
			    Attachment newPhoto = new Attachment();
			    newPhoto.ParentId = fanToUpdate.contactId;
                
			    newPhoto.Name = 'Fan Photo.Png'; 
    
                if ( fan.fanPhoto.photoName != null)
				    newPhoto.Name = fan.fanPhoto.photoName;                 
                
			    newPhoto.Body = encodedImage;
			    
			    if (fan.fanPhoto.contentType == null)
			    	fan.fanPhoto.contentType = 'image/png';
			    	
				newPhoto.ContentType = fan.fanPhoto.contentType;	    
			    
			    try {	        	
			    	insert newPhoto;
	        	}
	        	catch(FanServiceHelperException e){	            
	            	System.debug( 'Fan Service Helper : updateFan, Insert Photo ' + fanContact.Id );
	            	System.debug( 'Message ' + e.getMessage() );
	        	}
	        	finally 
	        	{
	        	}
	        	
	        	// The Fan photo got inserted fine, Now give them extra points for Uploading the photo if they have not already done so.
	        	
	        	system.debug( 'New photo Id ' + newPhoto.id );
	        	
	        	if( newPhoto.id <> null )
	        	{
            		
            		// If the photo has already been uploaded then skip, otherwise give the Fan extra points, check for Tier to see if the pointsh moved them to New Tier or not.
            		if ( ! fanContact.PhotoUploaded__c ) 
            		{  
	            		CSRA_EarningTransactionServiceHelper earningTranServiceHelper = new CSRA_EarningTransactionServiceHelper();
	
	        			CSRA_EarningServiceHelper earningServiceHelper = new CSRA_EarningServiceHelper();
			        
						CSRA_EarningAction earningActionForTier = earningServiceHelper.getEarningActionByName ('Upload Photo');
			
	        			system.debug( 'earningActionForTier ' + earningActionForTier.Id );
	        				
	        			earningTranServiceHelper.addEarningTransaction(earningActionForTier.Id);
	        
	        			CSRA_RewardTierServiceHelper rewardTierServiceHelper = new CSRA_RewardTierServiceHelper();
	                			
	        			// Anytime We add a transaction and set Fan tier, the points and Tier is likely to change. 
	        			// We need to retieve the following fields and update the curent fan so the values doesnt get wiped off.
	        			Contact c = rewardTierServiceHelper.setFanTierAndGivePoints();	
	        				        				
	        			fanToUpdate = getFan();
	        				
	        			fanContact.TotalPoints__c = c.TotalPoints__c ;	  
	        			fanContact.RemainingPoints__c = c.RemainingPoints__c;	
	        			fanContact.PointsUntilNextTier__c = c.PointsUntilNextTier__c;	
	        			fanContact.RewardTier__c = c.RewardTier__c;	
	        			fanContact.PhotoUploaded__c = true;
                    }
            		
			    	// send back photo information and delete previously inserted photos if the current insert was successful
			    
			    	fanToUpdate.fanPhoto.photoId = newPhoto.id;
				    fanToUpdate.fanPhoto.contentType = fan.fanPhoto.contentType;
				    fanToUpdate.fanPhoto.photoName = fan.fanPhoto.photoName; 
				    
				    // Display the inserted photo on the Contact Detail section
				    fanContact.PhotoURL__c  = '/servlet/servlet.FileDownload?file=' + newPhoto.id;
				    
				    List<Attachment> attachments = [ Select Id From Attachment Where ParentId = :fanToUpdate.contactId and Id <> :newPhoto.id];
				    
				    system.debug( 'attachments.size() ' + attachments.size() );
				   	
				    delete attachments;
			    }
			}	
		    else
	        {	            
		            System.debug( 'Fan Service Helper : updateFan -> No Contact record in the database for this user' );
		    }
	    }
	    else
        if (fan.isPhotoUpdated.toLowerCase() == 'false')	// Only update Fan data (mailing address, email but not photo.
	    {
	        if ( fanToUpdate != null )	// We have a valid Fan object to work on.
	        {		        		        
		        fanToUpdate.contactId                   = fan.contactId;
		        fanToUpdate.FirstName                   = fan.FirstName;
		        fanToUpdate.LastName                    = fan.LastName;
		        
		        if (( fan.Email != null ) && (fanContact.email != fan.email ))
		        {
			        fanToUpdate.Email                   = fan.Email;
			        fanContact.email            		= fan.email;
		        }
		    
		        if (( fan.Title != null ) && (fanContact.Title != fan.Title ))
		        {
		        	System.debug( ' Change title to -> ' + fanContact.Title );
			        fanToUpdate.Title          			= fan.Title; 
		        	fanContact.Title            		= fan.Title;
		        }
		                 
		        if (( fan.Phone != null ) && (fanContact.Phone != fan.Phone))
		        {
		        	fanToUpdate.Phone                   = fan.Phone;
		        	fanContact.Phone            		= fan.Phone;  
		        }
		        
		        if (( fan.mailingStreet != null ) && (fanContact.mailingStreet != fan.mailingStreet))
		        {
		        	fanToUpdate.mailingStreet           = fan.mailingStreet; 
		        	fanContact.mailingStreet    		= fan.mailingStreet; 
		        }
		        	
		        if (( fan.mailingState != null ) && (fanContact.mailingState != fan.mailingState))
		        {
			        fanToUpdate.mailingState            = fan.mailingState; 
		        	fanContact.mailingState     		= fan.mailingState;
		        }
		        
		        if (( fan.mailingPostalCode != null ) && (fanContact.mailingPostalCode != fan.mailingPostalCode))
		        {               		        	
		        	fanToUpdate.mailingPostalCode           = fan.mailingPostalCode;
		        	fanContact.mailingPostalCode			= fan.mailingPostalCode;  
		        }
		        
		        if (( fan.mailingCountry != null ) && (fanContact.mailingCountry != fan.mailingCountry))
		        {                  
		        	fanToUpdate.mailingCountry          = fan.mailingCountry;     
			    	fanContact.mailingCountry   		= fan.mailingCountry;  
		        }
		            	 
		        if (( fan.mailingCity != null ) && (fanContact.mailingCity != fan.mailingCity ))	 
		        {                 
		        	fanToUpdate.mailingCity             = fan.mailingCity;   
		        	fanContact.mailingCity      		= fan.mailingCity;
		        }
		        
		        if (( fan.gender != null )	&& (fanContact.gender__c != fan.gender))
		        {                  
			        fanToUpdate.gender                  = fan.gender;  
		        	fanContact.gender__c        		= fan.gender;   
		        }
		                 
		        if (( fan.birthDate != null ) && (fanContact.birthDate != fan.birthDate))
		        {
		        	fanToUpdate.birthDate               = fan.birthDate;                    
		       		fanContact.birthDate        		= fan.birthDate;
		        }
		        
	        	fanToUpdate.redeemablePoints             	= fanContact.remainingPoints__c;
	        	fanToUpdate.pointsUntilNextTier 			= fanContact.PointsUntilNextTier__c.IntValue();
	        	fanToUpdate.totalPoints             		= fanContact.totalPoints__c;  		        
		        
		        if ( fan.LastName != null )
		        	fanContact.LastName         			= fan.LastName;
		        else
		        	System.Debug( 'Null Last Name Will cause an exception!' );
		        
		        fanContact.FirstName        			= fan.FirstName;
		        
		        // Logic to see if the Fan completed the profile (Has data in each and every field, note- First Name, Last Name and Email is mandatory)
		        // If we have already given points for completed photo then skip, otherwise give the Fan extra points, check for Tier to see if the pointsh moved them to New Tier or not.		        
		        if(  (! fanContact.ProfileCompleted__c) && ( fan.phone <> '' ) && ( fan.mailingStreet <> '' ) && ( fan.mailingCity <> '' )
		        	&& ( fan.mailingStreet <> '' ) && ( fan.gender <> '' ) && ( fan.firstName <> '' ) && ( fan.birthDate <> null ) )
		        {
            		CSRA_EarningTransactionServiceHelper earningTranServiceHelper = new CSRA_EarningTransactionServiceHelper();

        			CSRA_EarningServiceHelper earningServiceHelper = new CSRA_EarningServiceHelper();
		        
					CSRA_EarningAction earningActionForTier = earningServiceHelper.getEarningActionByName ('Complete Profile');
		
        			earningTranServiceHelper.addEarningTransaction(earningActionForTier.Id);
        
        			CSRA_RewardTierServiceHelper rewardTierServiceHelper = new CSRA_RewardTierServiceHelper();
                			
        			// Anytime We add a transaction and set Fan tier, the points and Tier is likely to change. 
        			// We need to retieve the following fields and update the curent fan so the values doesnt get wiped off.
        			Contact c = rewardTierServiceHelper.setFanTierAndGivePoints();
        			
        			fanToUpdate = getFan();	
        				
        			fanContact.TotalPoints__c = c.TotalPoints__c;	  
        			fanContact.RemainingPoints__c = c.RemainingPoints__c;	
        			fanContact.PointsUntilNextTier__c = c.PointsUntilNextTier__c;	
        			fanContact.RewardTier__c = c.RewardTier__c;	
                    fanContact.ProfileCompleted__c = true;
        				
        			fanToUpdate.totalPoints = c.TotalPoints__c;	  
        			fanToUpdate.redeemablePoints = c.RemainingPoints__c;	  
        			fanToUpdate.pointsUntilNextTier = c.PointsUntilNextTier__c.IntValue();	
        			
		        }
	    	}
	    }
		
        try {
        	// try to update the fan contact data
            update fanContact;
        }
        catch(FanServiceHelperException e){
            
            System.debug( 'Fan Service Helper : updateFan ' + fanContact.Id );
            System.debug( 'Message ' + e.getMessage() );
        }
        finally {
        }
	
        return fanToUpdate;
    }

}