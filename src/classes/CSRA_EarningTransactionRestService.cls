@RestResource(urlMapping='/CSRA/EarningTransaction/*')

global with sharing class CSRA_EarningTransactionRestService {

/*******************************************************************************************************************
	Action POST:
	
	End point : /services/apexrest/CSRA/EarningTransaction/
	
	Or Sandbox end point : https://cs22.salesforce.com/services/apexrest/CSRA/EarningTransaction/
	
	Given an Earning Action Id, it builds the transaction and Inserts the record for the Current User.
	
	Request body:
	{
	"earningActionId" : "a0P17000000Bfh0"
	}
	
	"earningActionId" :
	
	Take a Tour 			30 points 	a0P17000000BfhK
	Buy Parking Pass		50 points	a0P17000000BfhZ	

*******************************************************************************************************************/
    
    @HttpPost
    global static CSRA_Fan postEarningActionTransactions(Id earningActionId) {        
        
        CSRA_FanServiceHelper fanServiceHelper = new CSRA_FanServiceHelper();
        
        CSRA_EarningTransactionServiceHelper earningTranServiceHelper = new CSRA_EarningTransactionServiceHelper();
        
        earningTranServiceHelper.addEarningTransactionSetTier(earningActionId);
        
        return fanServiceHelper.getFan();
    }
}