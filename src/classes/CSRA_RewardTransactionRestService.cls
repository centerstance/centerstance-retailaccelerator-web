@RestResource(urlMapping='/CSRA/RewardTransaction/*')

/*

               {
               "rewardItemId" : "a0O170000009OXk"
               }
 
Meet the players : a0O170000009OXd
 
Authentic Team Banner : a0O170000009OXf

*/

global with sharing class CSRA_RewardTransactionRestService {
    @HttpPost
    global static CSRA_Fan postRewardItemTransactions(Id rewardItemId) {
     
        CSRA_Fan fan = new CSRA_Fan();
        
        CSRA_RewardsServiceHelper earningTranServiceHelper = new CSRA_RewardsServiceHelper();
        
        fan = earningTranServiceHelper.addRewardTransaction(rewardItemId);
        
        return fan;  
    }
}