@RestResource(urlMapping='/CSRA/EarningActions/*')

global with sharing class CSRA_EarningRestService {

/*******************************************************************************************************************
	Action GET :
	
	End point : /services/apexrest/CSRA/EarningActions/
	
	Or Sandbox end point : https://cs22.salesforce.com/services/apexrest/CSRA/EarningActions/
	
	Gets a List of Earning Actions - which a Fan could take to Earn points.

*******************************************************************************************************************/
    
    @HttpGet
    global static List<CSRA_EarningAction> getEarningActions() {
     
        List<CSRA_EarningAction> earningActions = new List<CSRA_EarningAction>();
        
        CSRA_EarningServiceHelper EarningServiceHelper = new CSRA_EarningServiceHelper();
        
        earningActions = EarningServiceHelper.getEarningActions();
        
        return earningActions;  
	}
}