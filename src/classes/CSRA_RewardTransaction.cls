public with sharing class CSRA_RewardTransaction{
	// This class holds data bout Reward Transactions - Fan Redeemed this reward and this is the transaction for it.
	public String id; 
	public String rewardTransactionName;
	public String redeemedPoints;
	public DateTime redeemedDateTime;
}