@IsTest
private class CSRA_FanServiceHelperTest {

    enum PortalType { CSPLiteUser, PowerPartner, PowerCustomerSuccess, CustomerSuccess }
    
    testmethod static void testAll() {
        
        User userWithRole = null;
        Contact contact = null;
        User portalUser = null;
        
        userWithRole = createUserWithRole();
        
        system.runAs(userWithRole) {
			contact = createTestData();
        }
        
        portalUser = createPortalUser(contact.id);
         
        system.RunAs(portalUser) {
        	
            system.assert([select isPortalEnabled from user where id = :UserInfo.getUserId()].isPortalEnabled, 'User wasnt portal enabled within the runas block. ');
            
 	        Test.startTest();

			testFanGet();
	        testFanUpdate();
            testEarningAction();
            testEarningTransaction();
            testContent();
            testRewardTransaction();
	        
	        Test.stopTest();
        }
    }
   
    // Test data creation
    
    private static User createUserWithRole() {
    	
    	User userWithRole = null;
    	
        // Make sure the running user has a role otherwise an exception will be thrown.
        
        if(UserInfo.getUserRoleId() == null) {
			
            UserRole r = new UserRole(name = 'TEST ROLE');
            Database.insert(r);
            
            userWithRole = new User(alias = 'hasrole', email='userwithrole@StrideWell.com', userroleid = r.id,
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com');
        } else {
            userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
        }
            
        system.assert(userWithRole.userRoleId != null, 'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        
        return userWithRole;
    }
    
    private static Contact createTestData() {
    	
        Account account = new Account(name = 'TEST ACCOUNT');
        Database.insert(account);
        
        Contact contact = new Contact(AccountId = account.id, lastname = 'lastname', firstname = 'firstname', PointsUntilNextTier__c = 100, TotalPoints__c = 200 );
        Database.insert(contact);
        
        system.assertNotEquals(null, contact.id);
        
        addAttachmentToParent(contact.id);
        
        system.debug( 'Contact Id at Test creation ' + contact.id );
        
    	createEarningActions();
        createContactEarningTransactions(contact.id);
    	createRewardTiers(contact);
    	
    	return contact;
    }
    
    private static User createPortalUser(Id contactId) {
    	
    	// Get any profile for the given type.
        Profile profile = [select id 
                      		from profile 
                     		where usertype = :PortalType.PowerPartner.name() 
                     		limit 1];
        
        String testemail = 'puser003@StrideWell.com';
        User portalUser = new User(profileId = profile.id, username = testemail, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cstride', lastname='lastname', contactId = contactId);
        Database.insert(portalUser);
        
        system.assertNotEquals(null, portalUser.id);
        system.assert([select isPortalEnabled from user where id = :portalUser.id].isPortalEnabled, 'User was not flagged as portal enabled.');
        
        return portalUser;
    }
    
    private static void addAttachmentToParent(Id parentId) {  

	    Blob b = Blob.valueOf('Test Data');  
	      
	    Attachment attachment = new Attachment();  
	    attachment.ParentId = parentId;  
	    attachment.Name = 'Test Attachment for Parent';  
	    attachment.Body = b;  
	      
	    insert(attachment);  
	    
	    system.Assert(attachment.Id != null, 'Photo did not insert.');
	}  
    
    private static void createContactEarningTransactions(Id contactId) {
    	    	
    	List<ContactEarningTransaction__c> cetList = new List<ContactEarningTransaction__c>();
    	
    	ContactEarningTransaction__c cet = new ContactEarningTransaction__c( name = 'Texas chicken and waffles',  contact__c = contactId, EarnedPoints__c = 200 );
    	
    	cetList.Add(cet);
    	
    	ContactEarningTransaction__c cet1 = new ContactEarningTransaction__c( name = 'Ribs',  contact__c = contactId );
    	
    	cetList.Add(cet1);

    	insert cetList;    	    	
    }
    
    /*
    private static void createRewardTransactionsForBronzeTier(Id contactId) {
    	    	
    	List<ContactRewardTransaction__c> crtList = new List<ContactRewardTransaction__c>();
    	
    	ContactRewardTransaction__c crt = new ContactRewardTransaction__c( name = 'Texas chicken and waffles',  contact__c = contactId );
    	
    	crtList.Add(crt);
    	
    	ContactRewardTransaction__c crt1 = new ContactRewardTransaction__c( name = 'Ribs',  contact__c = contactId );
    	
    	crtList.Add(crt1);

    	insert crtList;    	
    	
    }
    //*/
    
    private static void createRewardItems() {
        
        DisplayData__c displayData = new DisplayData__c(
			RedColorCode__c=100,
			GreenColorCode__c=100,
			BlueColorCode__c=100
		);
		Database.insert(displayData);
    	
    	RewardItem__c rwdItems = new RewardItem__c( Name__c = 'Restaurant Pass', Points__c = 50, IsActive__c = true,
                                                            EffectiveDate__c = Date.today(), ExpirationDate__c = Date.today(), Order__c = 1, DisplayData__c=displayData.id );
    	insert rwdItems;
    }    
    
    private static void createEarningActions() {
    	  
		DisplayData__c displayData = new DisplayData__c(
			RedColorCode__c=100,
			GreenColorCode__c=100,
			BlueColorCode__c=100
		);
        Database.insert(displayData);
        
        EarningAction__c earnAction = new EarningAction__c( Name__c = 'Complete Profile', IsActive__c = true,  EffectiveDate__c = Date.today(), 
                                                           ExpirationDate__c = Date.today(), Points__c = 50, Order__c = 1, DisplayData__c=displayData.id );                  
    	insert earnAction;    
        
        EarningAction__c earnAction1 = new EarningAction__c( Name__c = 'Upload Photo', IsActive__c = true,  EffectiveDate__c = Date.today(), 
                                                           ExpirationDate__c = Date.today(), Points__c = 50, Order__c = 1, DisplayData__c=displayData.id );  
    	insert earnAction1;  
 
        
        EarningAction__c earnAction2 = new EarningAction__c( Name__c = 'Silver Warrior', IsActive__c = true,  EffectiveDate__c = Date.today(), 
                                                           ExpirationDate__c = Date.today(), Points__c = 50, Order__c = 1, DisplayData__c=displayData.id );  
    	insert earnAction2;          
 
        EarningAction__c earnAction3 = new EarningAction__c( Name__c = 'Gold Victor', IsActive__c = true,  EffectiveDate__c = Date.today(), 
                                                           ExpirationDate__c = Date.today(), Points__c = 50, Order__c = 1, DisplayData__c=displayData.id );  
    	insert earnAction3;          
        
        EarningAction__c earnAction4 = new EarningAction__c( Name__c = 'Sign Up', IsActive__c = true,  EffectiveDate__c = Date.today(), 
                                                           ExpirationDate__c = Date.today(), Points__c = 50, Order__c = 1, DisplayData__c=displayData.id );  
    	insert earnAction4;     
    }  
    
    private static void createRewardTiers(Contact contact) {

        RewardTier__c rewardTierSilver = new RewardTier__c( AchievementName__c = 'Silver', Name = 'Silver Warrior', ThresholdPoints__c = 200, Order__c = 1);
        insert rewardTierSilver;
    	
    	RewardTier__c rewardTierGold = new RewardTier__c( AchievementName__c = 'Gold', Name = 'Gold Victor', ThresholdPoints__c = 400, Order__c = 2);
        insert rewardTierGold;
        
        contact.rewardTier__c = rewardTierSilver.id;
    }
    
    // Test methods
    
    private static void testFanGet() {
    	
//        RestRequest req = new RestRequest(); 
//        RestResponse res = new RestResponse();
//        req.requestURI = '/services/apexrest/CSRA/Fan/';
//        req.httpMethod = 'GET';
//        RestContext.request = req;
//        RestContext.response = res;
        	
        CSRA_Fan myFan = new CSRA_Fan();
        myFan = CSRA_FanRestService.getFan();
        system.assertEquals(myFan.firstname, 'firstname');
    }
    
    private static void testFanUpdate() {
    	
        // Test fan update for basic data
        CSRA_Fan myFan = new CSRA_Fan();
        myFan.isPhotoUpdated = 'False';
        myFan.lastName = 'Changed';
        myFan.firstName = 'Changed';
        myFan.Email = 'Changed@hotmail.com';
        myFan.Title = 'Changed';   	        
   	    myFan.Phone = '972-000-0000';
        myFan.mailingStreet = '883 Changed st';
        myFan.mailingCity = 'Burbank';
        myFan.mailingState = 'CA';
   	    myFan.mailingPostalCode = '90248'; 
   	    myFan.mailingCountry = 'USA'; 
   	    myFan.gender = 'Male'; 
		myFan.birthDate = date.today().addDays(-100000);
        //myFan.PointsUntilNextTier__c = '100';
   	    CSRA_Fan returnedFan = CSRA_FanRestService.updateFan(myFan);
        system.assertEquals(returnedFan.mailingStreet, myFan.mailingStreet);   	        
	        
        // Test fan update for photo
        myFan.isPhotoUpdated = 'True';
        CSRA_FanRestService.updateFan(myFan);
    }
    
    private static void testEarningAction() {
    	
        CSRA_EarningRestService.getEarningActions();
    }
    
    private static void testEarningTransaction() {
    	
        CSRA_EarningServiceHelper earningServiceHelper = new CSRA_EarningServiceHelper();
        
        CSRA_EarningAction earningAction = earningServiceHelper.getEarningActionByName ('Sign Up');
//    	earningTransactionHelper.addEarningTransactionSetTier ( earningAction.Id ); 
        CSRA_EarningTransactionRestService.postEarningActionTransactions(earningAction.id);

        CSRA_EarningTransactionServiceHelper earningTransactionHelper = new CSRA_EarningTransactionServiceHelper();
        earningAction = earningServiceHelper.getEarningActionByName ('Upload Photo');
    	earningTransactionHelper.addEarningTransactionSetTier ( earningAction.Id ); 
        earningTransactionHelper.addEarningTransactionForTier( 'Silver Warrior' );
    }
    
    private static void testContent() {
  	
    	MarketingContent__c mktContent = new MarketingContent__c( 
    		Title__c = 'Experience the latest action.', 
    		Gender__c = 'Male', 
    		EffectiveDate__c = Date.today(), 
    		ExpirationDate__c = Date.today(), 
    		IsActive__c = true, 
    		MinimumAge__c = 0, 
    		MaximumAge__c = 100);
        insert mktContent;
        
        CSRA_ContentServiceHelper content = new CSRA_ContentServiceHelper();
        content.getMainContent();

    	CSRA_Fan fan = new CSRA_FanServiceHelper().getFan();
        fan.gender = 'Male';
		fan.birthDate = date.today().addDays(-10957); // about 30 years old
   	    CSRA_FanRestService.updateFan(fan);
		
//        RestRequest req = new RestRequest(); 
//        req.requestURI = '/services/apexrest/CSRA/Content/';
//        RestContext.request = req;
//        RestResponse res = new RestResponse();
//       req.httpMethod = 'GET';
//      RestContext.response = res;
        
        CSRA_ContentRestService.getMainContent();
    }
    
    private static void testRewardTransaction() {

        createRewardItems();
        
        CSRA_RewardRestService.getRewardItems();
//        CSRA_RewardsServiceHelper rewardServiceHelper = new CSRA_RewardsServiceHelper();
//        rewardServiceHelper.getRewardItems();            
        
        List<RewardItem__c> rwdList = [select id from RewardItem__c Limit 1];
//        postRewardItemTransactions(rwdList[0].Id);
    	CSRA_RewardTransactionRestService.postRewardItemTransactions( rwdList[0].Id );
    	
    	/*
    	List<ContactRewardTransaction__c> crtList = new List<ContactRewardTransaction__c>();
    	ContactRewardTransaction__c crt = new ContactRewardTransaction__c( name = 'Texas chicken and waffles',  contact__c = c.id );
    	crtList.Add(crt);
    	ContactRewardTransaction__c crt1 = new ContactRewardTransaction__c( name = 'Ribs',  contact__c = c.id );
    	crtList.Add(crt1);
    	insert crtList;
    	//*/
    }
}