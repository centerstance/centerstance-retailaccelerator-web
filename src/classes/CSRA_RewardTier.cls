global with sharing class CSRA_RewardTier {
	// This class holds data about Rewards Tiers - There are 3 Tiers at the moment Bronze, Silver and Gold
	// Fans move up through the Tiers as they earn points.They also get extra points (motivation) for moving themselves into New Tiers.
	public Id id {get; set;}
	public String name {get; set;}
	public String achievementName {get; set;}
	public String achievementMessage {get; set;}
    public Integer thresholdPoints {get; set;}
	public Integer order {get; set;}    
}