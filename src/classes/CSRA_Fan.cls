global with sharing class CSRA_Fan extends CSRA_Person {
	// This class Holds data about a Fan, it Inherits from a Person class.
	// A Fan Redeems Rewards, Earns Points, Fan has a Profile photo.
	// Fan belongs to a Tier at any point in time
	
  public string isPhotoUpdated {get; set;}
  
  public List<CSRA_EarningTransaction> earningTransactions {get; set;}
  
  public List<CSRA_RewardTransaction> rewardTransactions {get; set;}
  
  public CSRA_FanPhoto fanPhoto  {get; set;}
  
  public Decimal totalPoints  {get; set;}  
   
  public Decimal redeemablePoints {get; set;}  
    
	public CSRA_RewardTier rewardTier  {get; set;} 
    
    public Integer pointsUntilNextTier {get; set;} 
    
}