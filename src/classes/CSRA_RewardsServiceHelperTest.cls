@IsTest
public with sharing class CSRA_RewardsServiceHelperTest {
    
    static testmethod void testGetEarningActions() {
      
    setUpTestData();

       Test.startTest();
        
        CSRA_RewardsServiceHelper helper = new CSRA_RewardsServiceHelper();
    
   
   
      
      Test.stopTest();
    }
    
    public static void setUpTestData() {

    DisplayData__c displayData = new DisplayData__c(
      RedColorCode__c=100,
      GreenColorCode__c=100,
      BlueColorCode__c=100
    );
    Database.insert(displayData);
    system.assertNotEquals(null, displayData.id);
    
    RewardItem__c rewardItem = new RewardItem__c(
      Name__c='name', 
      Description__c='desc', 
      IsActive__c=true, 
      EffectiveDate__c=date.today(), 
      ExpirationDate__c=date.today(),
      Points__c=40,
      Order__c=1,
      DisplayData__c=displayData.id
    );
    Database.insert(rewardItem);
    system.assertNotEquals(null, rewardItem.id);
    }
}