@RestResource(urlMapping='/CSRA/RewardItems/*') 

global with sharing class CSRA_RewardRestService {
    @HttpGet
    global static List<CSRA_RewardItem> getRewardItems() {
     
        List<CSRA_RewardItem> rewardItems = new List<CSRA_RewardItem>();
        
        CSRA_RewardsServiceHelper EarningServiceHelper = new CSRA_RewardsServiceHelper();
        
        rewardItems = EarningServiceHelper.getRewardItems();
        
        return rewardItems;  
	}
}