public without sharing class CSRA_EarningTransactionServiceHelper {

	//custom exception
    public class CSRA_EarningTransactionServiceHelperException extends CS_CustomException {}
    
    //system logging debug level
    private System.LoggingLevel debugLevel = LoggingLevel.INFO;
    
    //method to add an earning transaction for the fan
    public Contact addEarningTransactionSetTier (Id earningActionId)
    {  
        CSRA_EarningTransactionServiceHelper earningTranServiceHelper = new CSRA_EarningTransactionServiceHelper();
        
        earningTranServiceHelper.addEarningTransaction(earningActionId);

        CSRA_RewardTierServiceHelper rewardTierServiceHelper = new CSRA_RewardTierServiceHelper();
        
        Contact c = rewardTierServiceHelper.setFanTierAndGivePoints();
        
        try {	              
        	update c;
	        }
	    catch(CSRA_EarningTransactionServiceHelperException e){	            
	            	System.debug( 'addEarningTransactionSetTier error : earningActionId ' + earningActionId );
	            	System.debug( 'Message ' + e.getMessage() );           	
	    }
        
        return c;

    }
          
    public Contact addEarningTransaction (Id earningActionId)
    {  
    	//initialize
    	CSRA_FanServiceHelper fanServiceHelper = new CSRA_FanServiceHelper();
        
        if(string.valueof(earningActionId) == null || string.valueof(earningActionId) == ''){
        	throw new CSRA_EarningTransactionServiceHelperException('Missing parameter - earning action Id: ' + earningActionId);
        }        
        
        //get contact Id for the fan
        Id contactId = fanServiceHelper.getContactId();
        
        system.debug(debugLevel, 'ContactId ' + ContactId + ' EarningActionID ' + EarningActionID );
       
        //get the current points summary for the fan
        List<Contact> contacts = [	Select Id
        								,TotalPoints__c
        								,RemainingPoints__c
        								,PointsUntilNextTier__c
        								,RewardTier__c 
        							From Contact 
        							where Id =: contactId];
        
        //get earning action details
        
        List<EarningAction__c> earningAction = [SELECT Id, Name, EffectiveDate__c, ExpirationDate__c, Name__c, Points__c, Description__c FROM EarningAction__c Where IsActive__c = true and Id = :EarningActionID  And (EffectiveDate__c <= Today) And (ExpirationDate__c >= Today)];
        
        system.debug( 'earningAction retrieved ' + earningAction.get(0).Name__c + ' earningAction.get(0).Points__c ' + earningAction.get(0).Points__c );
        
       	if(!earningAction.IsEmpty()){
            
            try{   
            	Decimal Point = earningAction.get(0).Points__c;			
            }
			catch(CSRA_EarningTransactionServiceHelperException e){
			    System.debug( 'Earning Action Point has not been set up ' + e.getMessage() );
			}
			
            if (earningAction.get(0).Points__c == null)
            	earningAction.get(0).Points__c = 0;
            
            //the fan does not have any points assigned, initialize the values to zero as the first assignment.
            if (contacts.get(0).TotalPoints__c == null)
            	contacts.get(0).TotalPoints__c = 0;
            
            if (contacts.get(0).PointsUntilNextTier__c == null)
            	contacts.get(0).PointsUntilNextTier__c = 0;
            	
            if (contacts.get(0).RemainingPoints__c == null)
            	contacts.get(0).RemainingPoints__c = 0;            	
            
            //system.debug( ' setting up contactEarningTransaction for ' + earningAction.get(0).Name__c);
            
            ContactEarningTransaction__c contactEarningTransaction = new ContactEarningTransaction__c();
			contactEarningTransaction.name =  earningAction.get(0).Name__c;  
            contactEarningTransaction.Contact__c =  ContactId; 
            contactEarningTransaction.EarningActionId__c =  EarningActionID; 
            contactEarningTransaction.EffectiveDate__c =  earningAction.get(0).EffectiveDate__c;   
			contactEarningTransaction.ExpirationDate__c =  earningAction.get(0).ExpirationDate__c; 
			contactEarningTransaction.EarnedPoints__c = earningAction.get(0).Points__c;
			contactEarningTransaction.EarnedDateTime__c = Datetime.now();
			
			Decimal totalPoints = 0;
			totalPoints = contacts.get(0).TotalPoints__c + earningAction.get(0).Points__c; 
			contacts.get(0).RemainingPoints__c = contacts.get(0).RemainingPoints__c + earningAction.get(0).Points__c;             
            
            contacts.get(0).TotalPoints__c = totalPoints;
            
            Savepoint savePointEarningTransaction = Database.setSavepoint();            
            
            try {	
            	
            	System.debug('Contact EARNING TRANSACTION: ' + contactEarningTransaction);
            	System.debug('Contacts: ' + contacts);             
                insert contactEarningTransaction;
				update contacts;
				System.debug('Contact EARNING TRANSACTION AFTER: ' + contactEarningTransaction);
            	System.debug('Contacts AFTER: ' + contacts);    
	        }
	        catch(CSRA_EarningTransactionServiceHelperException e){	            
	            	System.debug( 'Earning Transaction Service Helper : Insert Earning Transaction, Contact ID ' + ContactId );
	            	System.debug( 'Message ' + e.getMessage() );
	        
	        	Database.rollback(savePointEarningTransaction);
	        	
	        	return null;	            	
	        }
	        finally 
	        {            
 	        }
        }                   
        
        return contacts.get(0);
    }

    public void addEarningTransactionForTier (String tierName)
    { 
		CSRA_EarningServiceHelper earningServiceHelper = new CSRA_EarningServiceHelper();
		        
		CSRA_EarningAction earningActionForTier = earningServiceHelper.getEarningActionByName (tierName);
		                
		addEarningTransaction ( earningActionForTier.Id );
    }    

}