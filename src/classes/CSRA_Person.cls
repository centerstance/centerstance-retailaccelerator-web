public without sharing abstract class CSRA_Person {
  // This class holds data about a Person, Eventually a Fan class inherits from it.
  
  public Id     	contactId			{ get; set; }
  public Id     	personAccountId 	{ get; set; }
  public String   	firstName       	{ get; set; }
  public String   	lastName       		{ get; set; }  
  public Date   	birthDate       	{ get; set; }
  public String   	gender        		{ get; set; }
  public String		title         		{ get; set; }
  public String   	phone         		{ get; set; }
  public String   	email         		{ get; set; }
  public String   	mailingStreet       { get; set; }
  public String   	mailingCity         { get; set; }
  public String   	mailingState        { get; set; }
  public String   	mailingPostalCode   { get; set; }
  public String   	mailingCountry      { get; set; }
  
  public String   fullName { 
    
    get {
        String fname = this.firstName == null ? '' : this.firstName;
        String lname = this.lastName == null ? '' : this.lastName;
        
        return (fname + ' ' + lname).trim();
      }  
    
  }

}