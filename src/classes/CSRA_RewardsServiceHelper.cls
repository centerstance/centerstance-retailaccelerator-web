public without sharing class CSRA_RewardsServiceHelper {

        //custom excetption
    public class RewardsServiceHelperException extends CS_CustomException {} 
    
    //system logging debug level
    private System.LoggingLevel debugLevel = LoggingLevel.INFO;    
    
    public CSRA_Fan addRewardTransaction (Id rewardItemId)
    {  
    	//initialize
    	CSRA_Fan fan = new CSRA_Fan(); 
    	CSRA_FanServiceHelper fanServiceHelper = new CSRA_FanServiceHelper();
    	
    	//validate input
        if(string.valueof(rewardItemId) == null || string.valueof(rewardItemId) == ''){
        	throw new RewardsServiceHelperException('Missing parameter - earning action Id: ' + rewardItemId);
        }
        
        //get contact Id for the fan
        Id contactId = fanServiceHelper.getContactId();
        
        system.debug(debugLevel, 'ContactId ' + ContactId + ' RewardItemId ' + rewardItemId );
       
        //get the current points summary for the fan
        List<Contact> contacts = [	Select Id
        								,TotalPoints__c
        								,RemainingPoints__c
        								,PointsUntilNextTier__c
        								,RewardTier__c 
        							From Contact 
        							where Id =: contactId];
        							
		List<RewardItem__c> rewardItems = 
					[
						SELECT 
							Id, 
							Name, 
							EffectiveDate__c, 
							ExpirationDate__c, 
							Name__c, Points__c, Description__c FROM RewardItem__c Where IsActive__c = true and Id = :rewardItemId  And (EffectiveDate__c <= Today) And (ExpirationDate__c >= Today)];
        
       	if(!rewardItems.IsEmpty()){
            
            try{   
            	Decimal Point = rewardItems.get(0).Points__c;			
            }
			catch(RewardsServiceHelperException e){
			    System.debug( 'Reward Item Point has not been set up ' + e.getMessage() );
			}
			
            if (rewardItems.get(0).Points__c == null)
            	rewardItems.get(0).Points__c = 0;
            
            //the fan does not have any points assigned, initialize the values to zero as the first assignment.
            if (contacts.get(0).TotalPoints__c == null)
            	contacts.get(0).TotalPoints__c = 0;
            
            if (contacts.get(0).PointsUntilNextTier__c == null)
            	contacts.get(0).PointsUntilNextTier__c = 0;
            
            if (contacts.get(0).RemainingPoints__c >= rewardItems.get(0).Points__c)
            {
	            ContactRewardTransaction__c rewardItemTransaction = new ContactRewardTransaction__c();
				rewardItemTransaction.name =  rewardItems.get(0).Name__c;  
	            rewardItemTransaction.Contact__c =  ContactId; 
	            rewardItemTransaction.RewardItem__c =  rewardItemId;  
				rewardItemTransaction.RedeemedPoints__c = rewardItems.get(0).Points__c;
				rewardItemTransaction.RedeemedDateTime__c = Datetime.now();
				
				contacts.get(0).RemainingPoints__c = contacts.get(0).RemainingPoints__c - rewardItems.get(0).Points__c; 
				
            	Savepoint savePointRewardTransaction = Database.setSavepoint();            
            
            	try {	              
                	insert rewardItemTransaction;
					update contacts;
	        	}
	        	catch(RewardsServiceHelperException e){	            
	            	System.debug( 'Reward Item Service Helper : Insert Reward Item Transaction, Contact ID ' + ContactId );
	            	System.debug( 'Message ' + e.getMessage() );
	        
	        		Database.rollback(savePointRewardTransaction);	            	
	        	}
	        	finally 
	        	{            
 	        	} 	        		
            }
      	}   

        fan =  fanServiceHelper.getFan();     	 	        

		return fan;        							
    }
        						
    //method to get all active earning actions
    public List<CSRA_RewardItem> getRewardItems () {      
        
        //initialize list
        List<CSRA_RewardItem> rewardItems = new List<CSRA_RewardItem>();
        
        //get the list of active reward items from DB
           List<RewardItem__c> listRewardItems = [   Select Id
                                                                ,Name__c
                                                                ,Description__c
                                                                ,Points__c
                                                                ,Order__c
                                                                ,IsActive__c
                                                                ,IconURL__c                                                              
                                                                ,ExpirationDate__c
                                                                ,EffectiveDate__c
                                                                ,DisplayData__c
                                                                ,DisplayData__r.Name
                                                                ,DisplayData__r.LabelText__c
                                                                ,DisplayData__r.AboveLabelText__c
                                                                ,DisplayData__r.BelowLabelText__c
                                                                ,DisplayData__r.PrimaryText__c
                                                                ,DisplayData__r.HelpText__c
                                                                ,DisplayData__r.NotificationMessage__c
                                                                ,DisplayData__r.ButtonText__c
                                                                ,DisplayData__r.BlueColorCode__c
                                                                ,DisplayData__r.GreenColorCode__c
                                                                ,DisplayData__r.RedColorCode__c
                                                                ,DisplayData__r.HexColorCode__c
                                                            From RewardItem__c 
                                                            Where IsActive__c = true 
                                                            And (EffectiveDate__c <= Today) And (ExpirationDate__c >= Today) 
                                                            order by Order__c
                                                        ];

        //if we get reward items from DB
        if(!listRewardItems.IsEmpty()){
            
            //get the unique reward item ids 
            Map<Id, RewardItem__c> rewardItemsMap = (new Map<Id, RewardItem__c>(listRewardItems));
            Set<Id> rewardItemIds = rewardItemsMap.keySet();
            
            //system.debug( 'rewardActionsMap size ' + rewardItemsMap.size() + ' ' + rewardItemIds);         
            
            // Build the Map for attachment and its Body, so we can get it when looping through the list of Reward Items
            Map<Id,Attachment> mapRewardItemsIdByIcon = new Map<Id,Attachment>();
            
            for(Attachment icon:[ Select Id, ParentId, Name, Body 
                                  From Attachment 
                                  Where ParentId IN :rewardItemIds 
                                  and isDeleted = false 
                                ])
            {               
                //if the map does not have the parent id key
                if(!mapRewardItemsIdByIcon.containsKey(icon.ParentId)) {
                    
                    mapRewardItemsIdByIcon.put(icon.ParentId, icon);
                    //system.debug(debugLevel,  'Parent ' + icon.ParentId + ' icon ' + icon.name + ' Icon id ' +  icon.id);
                    
                } else {
                    
                    continue;
                }
            }
                     
            //iterate reward items and set attributes                    
            for ( RewardItem__c ri : listRewardItems){
                
                CSRA_RewardItem rewardItem = new CSRA_RewardItem();
                CSRA_DisplayData displayData = new CSRA_DisplayData();
                        
                rewardItem.id                = ri.id;    
                rewardItem.name              = ri.Name__c;
                rewardItem.description       = ri.Description__c;
                rewardItem.effectiveDate     = ri.EffectiveDate__c;  
                rewardItem.expirationDate    = ri.ExpirationDate__c;
                rewardItem.points            = ri.Points__c.intValue();
                rewardItem.displayOrder      = ri.Order__c.format();
                rewardItem.iconUrl           = ri.IconURL__c;
                
                //set corresponding display data
                displayData.labelText           = ri.DisplayData__r.LabelText__c;
                displayData.aboveLabelText      = ri.DisplayData__r.AboveLabelText__c;
                displayData.belowLabelText      = ri.DisplayData__r.BelowLabelText__c;
                displayData.primaryText         = ri.DisplayData__r.PrimaryText__c;
                displayData.helpText            = ri.DisplayData__r.HelpText__c;
                displayData.buttonText          = ri.DisplayData__r.ButtonText__c;
                displayData.notificationMessage = ri.DisplayData__r.NotificationMessage__c;
                displayData.hexColorCode        = ri.DisplayData__r.HexColorCode__c;
                displayData.redColorCode        = ri.DisplayData__r.RedColorCode__c.format();               
                displayData.greenColorCode      = ri.DisplayData__r.GreenColorCode__c.format(); 
                displayData.blueColorCode       = ri.DisplayData__r.BlueColorCode__c.format();
                rewardItem.displayData       = displayData;  
                
                // If there is a corresponding attachment to the Reward Items, then Get it from the Map.
                if (mapRewardItemsIdByIcon.containsKey(ri.id) ) {
                    
                    rewardItem.iconBinaryData  = EncodingUtil.base64Encode( mapRewardItemsIdByIcon.get(ri.id).Body);
                }
                else {
                    
                    rewardItem.iconBinaryData = '';
                    //system.debug( debugLevel, 'mapRewardItemsIdByIcon.get(ri.id) <> null ' + ri.id);
                }
                    
                //add to list   
                rewardItems.add(rewardItem);          
            }

        }
        else {
            
            throw new RewardsServiceHelperException('Unable to get reward items. Please contact system administrator.');
            system.debug (debugLevel,  'No Reward Items records found!' );                    
        }
        
        return rewardItems;  
    }

    
}