global with sharing class CSRA_EarningAction {
		// This class holds data baout Earning actions - These are the actions fans can take to earn points.
	  public Id 				id 					{get; set;}
	  public String 			name 				{get; set;}
      public String 			description 		{get; set;}	
	  public Date 				effectiveDate  		{get; set;}	
	  public Date 				expirationDate  	{get; set;}
	  public Integer  			points  			{get; set;}
	  public String 			frequency 			{get; set;}    
	  public String 			iconBinaryData 		{get; set;}
	  public String 			iconUrl				{get; set;}    
      public String 			displayOrder		{get; set;} 
      public CSRA_DisplayData 	displayData  		{get; set;}
      
}