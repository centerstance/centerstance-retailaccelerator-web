@RestResource(urlMapping='/CSRA/Content/*')

global with sharing class CSRA_ContentRestService {

/*******************************************************************************************************************
	Action GET :
	
	End point : /services/apexrest/CSRA/Content/

*******************************************************************************************************************/
    
    @HttpGet
    global static CSRA_Content getMainContent() {
    	
    	CSRA_ContentServiceHelper contentServiceHelper = new CSRA_ContentServiceHelper();
	    
	   	return contentServiceHelper.getMainContent();
	}
}