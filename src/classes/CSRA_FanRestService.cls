/*
*   
*   Test url : https://workbench.developerforce.com/restExplorer.php
*   Resource at : /services/apexrest/CSRA/Fan/*, Action PUT, Method : updateFan
*	Sandbox end point : https://cs22.salesforce.com/services/apexrest/CSRA/Fan/
*	When isPhotoUpdated flag is sent as False, Only data is updated.
*
*   Request Body for PUT :
*
{
    "fan" : 
    {
		  "title" : "Big Fan",
		  "phone" : "(972) 718-3164",
		  "personAccountId" : null,
		  "mailingStreet" : "6462 West Main st.",
		  "mailingState" : "TX",
		  "mailingPostalCode" : "75082",
		  "mailingCountry" : "USA",
		  "mailingCity" : "Dallas",
		  "lastName" : "Tester",
		  "gender" : "Male",
		  "fullName" : "Bobby Tester",
		  "firstName" : "Bobby",
		  "email" : "bigdfan@yahoo.com",
		  "birthDate" : "1994-08-17",
          "isPhotoUpdated" : "false"
     }

}

Action GET :

/services/apexrest/CSRA/Fan/


/*
******************************************************************************************************************
// Action PUT :
// Updates Fan information, To ubderstand more about this call please do a Get and JSON object will make more sense.
// GET End Point : /services/apexrest/CSRA/Fan/
// Fan data and photo information update is Mutually exclusive. When isPhotoUpdated flag is sent as False, Only data is updated.
// When sent a True only Photo information is updated. It accepts base64 encoded string for photo to be uploaded and downloaded.
*****************************************************************************************************************/

@RestResource(urlMapping='/CSRA/Fan/*')

global with sharing class CSRA_FanRestService {
    
    @HttpPut
    global static CSRA_Fan  updateFan(CSRA_Fan fan) {
        
        CSRA_FanServiceHelper fanSvcHelper = new CSRA_FanServiceHelper();
                
        CSRA_Fan updatedFan = new CSRA_Fan(); 
        
        updatedFan =  fanSvcHelper.updateFan(fan);
        
        return updatedFan;        
    }

/*******************************************************************************************************************
Action GET :

End point : /services/apexrest/CSRA/Fan/

Or Sandbox end point : https://cs22.salesforce.com/services/apexrest/CSRA/Fan/

*******************************************************************************************************************/
    @HttpGet
    global static CSRA_Fan getFan() {
     
        CSRA_FanServiceHelper fanSvcHelper = new CSRA_FanServiceHelper();
        CSRA_Fan fan = new CSRA_Fan(); 
        fan =  fanSvcHelper.getFan();
        
        return fan;  
    }
    	 
}