global with sharing class CSRA_RewardItem {
		// This class holds data about Reward Items - these are the rewards a Fan can get onve they have earned enough points to Redeem the reward.
	  public Id 				id 					{get; set;}
	  public String 			name 				{get; set;}
      public String 			description 		{get; set;}	
	  public Date 				effectiveDate  		{get; set;}	
	  public Date 				expirationDate  	{get; set;}
	  public Integer  			points  			{get; set;}
	  public String 			iconBinaryData 		{get; set;}
	  public String 			iconUrl				{get; set;}    
      public String 			displayOrder		{get; set;} 
      public CSRA_DisplayData 	displayData  		{get; set;}
}