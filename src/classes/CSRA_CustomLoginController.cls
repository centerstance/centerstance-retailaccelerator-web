global with sharing class CSRA_CustomLoginController {
	
    global String username {get;set;}
    global String password {get;set;}
    global String startURL {get; set;}
    
    global CSRA_CustomLoginController (){
    
    	startURL = System.currentPageReference().getParameters().get('startURL');
    	System.debug('Start URL: ' + startURL);
    }
    
    global PageReference forwardToCustomAuthPage(){
        return new PageReference('/CustomLogin');
    }
    global PageReference login(){
        return Site.login(username, password, startURL);
    }
}