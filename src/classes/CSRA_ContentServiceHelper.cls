public with sharing class CSRA_ContentServiceHelper {

	//custom exception
    public class ContentServiceHelperException extends CS_CustomException {} 
    
    //system logging debug level
    private System.LoggingLevel debugLevel = LoggingLevel.INFO;
    
    //method to get the main content data
    public CSRA_Content getMainContent() {
    	
    	//initialize
    	CSRA_Content content = new CSRA_Content();
    	
    	//get content from DB
    	List<MarketingContent__c> contentFromDB = [	Select Id
    													, Blurb__c
    													, Title__c
    													, Gender__c
    													, MinimumAge__c
    													, MaximumAge__c
    													, IsDefaultContent__c
    													, ImageURL__c
    												From MarketingContent__c
    												Where IsActive__c = true
    												AND (EffectiveDate__c <= Today) And (ExpirationDate__c >= Today)
    											  ];
    											  
    	system.debug( 'contentFromDB ' + contentFromDB );
    	
    	//get fan data
    	CSRA_Fan fan = new CSRA_FanServiceHelper().getFan();
    	
    	//check to see if the criteria is set on the fan
    	Boolean isGenderSet;
		Boolean isBirthDateSet;
		Integer fanAge = 0;
    				
		//if the fan's gender is set
		if(fan.gender != null && (fan.gender.equalsIgnoreCase('Male') || fan.gender.equalsIgnoreCase('Female'))){
			isGenderSet = true;
		} else {
			isGenderSet = false;
		}
		
		//if the fan birthdate is set and it is less than today
		if(fan.birthDate != null && fan.birthDate < system.today()){
			
			//enable the flag
			isBirthDateSet = true;
			
			//and calculate the age
			Integer totaldays = fan.birthDate.daysBetween(system.today());
			fanAge = (Integer)(math.Floor(totaldays/365.2425));
		}
		
    	//iterate the content and find the one that meets the current user's criteria
    	if(!contentFromDB.isEmpty()){
    		
    		//initialize
    		MarketingContent__c defaultContent = new MarketingContent__c();
    		MarketingContent__c fanSpecificContent = new MarketingContent__c();
    		
    		for(MarketingContent__c mc : contentFromDB){
    			
    			//find the default content from the list
    			if(mc.IsDefaultContent__c){
    				
    				defaultContent = mc;
    				
    			} else {
    				
    				//find the right content based on the fan criteria
    				if(isGenderSet && isBirthDateSet){
    					
    					if(mc.Gender__c == fan.gender && (mc.MinimumAge__c <= fanAge && mc.MaximumAge__c >= fanAge)){
    						fanSpecificContent = mc;
    					}
    				}
    				
    			}
    		
    		}
    		
    		//check to see if the fan specific data is set
    		if(fanSpecificContent.Id != null){
    			
    			content.id 				= fanSpecificContent.Id;
				content.title 			= fanSpecificContent.Title__c;
				content.blurb			= fanSpecificContent.Blurb__c;
				content.imageURL 		= fanSpecificContent.ImageURL__c;
    			
    		} else {
    			
    			content.id 				= defaultContent.Id;
				content.title 			= defaultContent.Title__c;
				content.blurb			= defaultContent.Blurb__c;
				content.imageURL 		= defaultContent.ImageURL__c;
    			
    		} 
    		
    		//get the image data from the DB
    		for(Attachment image :[ Select Id, ParentId, Name, Body 
                                  From Attachment 
                                  Where ParentId =: content.id  
                                  and isDeleted = false 
                                ]) {
                                	
                   content.imageId = image.Id;
                   content.imageBinaryData = EncodingUtil.base64Encode(image.body);
                                	
            }
    		
    	} else {
    		
    		throw new ContentServiceHelperException('Unable to get content. Please contact system administrator.');
    	}
    	
    	//return to caller
    	return content;
    }
    
    
}