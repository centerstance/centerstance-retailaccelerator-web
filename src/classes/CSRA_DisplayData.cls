global with sharing class CSRA_DisplayData {
	
	// This class holds Display data that goes along wth the Earning Actions and Reward Items List.
	
	  public String id {get; set;}
	  
	  public String labelText {get; set;}
	  
	  public String aboveLabelText {get; set;}
	  
	  public String belowLabelText {get; set;}
	  
	  public String primaryText {get; set;}
	  
	  public String helpText {get; set;}	 
	   
      public String buttonText {get; set;}
      
      public String notificationMessage {get; set;}
      
      public String hexColorCode {get; set;}
        
      public String redColorCode {get; set;}
        
      public String greenColorCode {get; set;}
        
      public String blueColorCode {get; set;} 
      
         
}