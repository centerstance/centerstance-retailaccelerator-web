@IsTest
private class CSRA_TriggerOnUserTest {

    static private Contact portalUserContact;
    static private Contact portalUserContact1;
    
    static User portalAccountOwner1 ;
                
    static testmethod void testUserTrigger() { 
                                                                  
    setUpAccountAndContact(True);    
}
        
public static void setUpAccountAndContact(boolean InsertContacts) {
        
	User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

    System.runAs (thisUser) {
                         
    UserRole portalRole = new UserRole(name = 'TEST ROLE');
    Database.insert(portalRole);

    Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
	// Account owner must have a role otherwise - UNKNOWN_EXCEPTION, portal account owner must have a role: []
    portalAccountOwner1 = new User(
    	UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'testbat@test.com',
        Alias = 'BH',
        Email='BobbyHoque@Tester.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bobby',
        Lastname='Hoque',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
	);
            
	Database.insert(portalAccountOwner1);

    // Very important that Account owner is a Portal User. 
    // Otherwise System.DmlException: Insert failed. First exception on row 0; first error: UNKNOWN_EXCEPTION, portal account owner must have a role: []
    
    Account portalAccount1 = new Account(
	    Name = 'TestAccount',
        OwnerId = portalAccountOwner1.Id
    );
    
    Database.insert(portalAccount1);
            
    if ( InsertContacts )
    {        
        portalUserContact = new Contact(AccountId = portalAccount1.id, lastname = 'lastname', firstname = 'firstname');
        insert portalUserContact;
                
        // Creating 2 contact and users to test the Bulk processing of the trigger
        portalUserContact1 = new Contact(AccountId = portalAccount1.id, lastname = 'lastname1', firstname = 'firstname1');
        insert portalUserContact1;
    }
    
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
	}
    
    // Now that all the setup data is there, create a user from as if the portal user is registering
        
    System.RunAs(portalAccountOwner1) 
    {
                          
    	Test.startTest();
           
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Portal User%' Limit 1];
        
        //  Make sure the user's profile is a Portal User profile
        //  
        List<User> Users = new List<User> ();
            
        User user1 = new User(
        	// Millisec avoids duplicate user issue, still taking it out for now.
        	Username = 'bobby_plano@test.com',
            ContactId = portalUserContact.Id,
            ProfileId = portalProfile.Id,
            Alias = 'bobbyp',
            Email = 'bobby_plano@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Hoque',
            CommunityNickname = 'Bobby',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
      	);
        
        Users.add(user1);
           
        User user2 = new User(
        	
        	Username = 'bobby_plano1@test.com',
            ContactId = portalUserContact1.Id,
            ProfileId = portalProfile.Id,
            Alias = 'bobbyp1',
            Email = 'bobby_plano1@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Hoque',
            CommunityNickname = 'Bobby1',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
      	);

        Users.add(user2);           
		
        // Insert list of users to batch multiple users for testing the Bulk side of the trigger
        Database.insert(Users);
                
        Set<Id> ids = (new Map<Id, User>(Users)).keySet();
        
        Set<Id> contactIds = new Set<Id>(); 
        
		// If any of the Portal user hasnt been enabled for the user, assert
        
        for ( User u : [select Id, Name, isPortalEnabled, contactId
                          from user 
        	              where id In :ids] )
        {
			System.assert(u.isPortalEnabled, 
                         'User wasnt portal enabled within the runas block. User Name - ' + u.Name ); 
            
            contactIds.add(u.ContactId);            
        }           
        
        for ( Contact c : [select Id, Name, UserLoggedInForFirstTime__c
                          from Contact 
        	              where id In :contactIds] )
        {
			System.assert(c.UserLoggedInForFirstTime__c, 
                         'User Logged in For first time flag error. Contact Name - ' + c.Name ); 

            System.debug( 'User Logged in Flag. ' + c.UserLoggedInForFirstTime__c + ' User Name - ' + c.Name ); 
        }                        
        
	    Test.stopTest();                          
                          			                          
        }        
    }        
}