public without sharing class CSRA_RewardTierServiceHelper {

	//custom exception
    public class CSRA_RewardTierServiceHelperException extends CS_CustomException {}
    
    public List<CSRA_RewardTier> getRewardTiers()
    { 
        List<CSRA_RewardTier> rewardTiersList = new List<CSRA_RewardTier> (); 
        // Get all the reward tiers given the filter criteria
        List<RewardTier__c> rewardTiers = [SELECT Id, IsDeleted, Name, ThresholdPoints__c FROM RewardTier__c where IsDeleted = false Order By ThresholdPoints__c Desc];
        
        if ( ! rewardTiers.isEmpty() )
        {
            for( RewardTier__c rewardTier : rewardTiers)
            {
                CSRA_RewardTier rwdTier = new CSRA_RewardTier();
                rwdTier.id = rewardTier.Id;
                rwdTier.name = rewardTier.Name;
                rwdTier.thresholdPoints = rewardTier.ThresholdPoints__c.intValue();
                
                rewardTiersList.add(rwdTier);
            }
        }
        
        return rewardTiersList;
    }
    
	// Compares the current Tier on a Fan vs.Total points they have, and moves them up a Tier if applicable. Also adds Transactions (points) for crossing the Tier.
    public contact setFanTierAndGivePoints(  )
    {
    	CSRA_FanServiceHelper fanServiceHelper = new CSRA_FanServiceHelper();
    	Id contactId = fanServiceHelper.getContactId();
               
        //get the current points summary for the fan
        List<Contact> contacts = [	Select Id
        								,TotalPoints__c
        								,RemainingPoints__c
        								,PointsUntilNextTier__c
        								,RewardTier__c 
        							From Contact 
        							where Id =: contactId];
        							
		Contact c = contacts.get(0);        							
        							    	
        List<CSRA_RewardTier> rewardTiersList = new List<CSRA_RewardTier> (); 
            
        CSRA_RewardTierServiceHelper rewardTierHelper = new CSRA_RewardTierServiceHelper();
        rewardTiersList = rewardTierHelper.getRewardTiers();
                        
        CSRA_RewardTier rewardTierFanWillBeAssigned = new CSRA_RewardTier ();
            
        // Logic below has been made generic so that, even if we add more tiers it should function properly.
        Integer index = 0;
        CSRA_RewardTier higherTier;
        decimal pointsUntilNextTier;
           
        // Find out which tier the Fan belongs to, Start from the highest to the lowest.
        for( CSRA_RewardTier rwdTier : rewardTiersList )				
        {
          	system.debug( 'contacts.get(0).TotalPoints__c ' + c.TotalPoints__c + ' rwdTier.thresholdPoints ' + rwdTier.thresholdPoints );
            	
           	if(c.TotalPoints__c >= rwdTier.thresholdPoints )
           	{
           		rewardTierFanWillBeAssigned = rwdTier;
            		            		            	
           		if (index == 0)	// we are dealing with the highset tier, in that case Point until next doesnt apply.
           		{
           			pointsUntilNextTier = 0;
           		}
				else
           		{
           			system.debug( 'rwdTier.name ' + rwdTier.name + ' higherTier.name ' + higherTier.name + 'PointsNeededForThisTier( rwdTier.name, rewardTiersList ) ' + PointsNeededForThisTier( rwdTier.name, rewardTiersList ) );
           			pointsUntilNextTier = PointsNeededForThisTier( higherTier.name, rewardTiersList );
           		}
					            		
           		break;
           	}
            	
           	higherTier = rwdTier;
            	
           	index++;
        }
                        
        string currentRewardTier;
        if ( c.RewardTier__c <> null )
        {
           	currentRewardTier = c.RewardTier__c;
        }
        else
        {
           	currentRewardTier = null;
        }   
            
        system.debug( 'rewardTierFanWillBeAssigned ' + rewardTierFanWillBeAssigned + ' contacts.get(0).RewardTier__c ' + c.RewardTier__c);         
                       
        Decimal pointsNeededForNextHigherTier = 0;
            
        // If Fan is moving on to a New Tier, give the Fan some extra points for going in to the New Tier, also assign them the new tier Id for contact object
        	        
        if ( ( currentRewardTier <> rewardTierFanWillBeAssigned.Id ) ) 
        {
          	c.RewardTier__c = rewardTierFanWillBeAssigned.Id;
            	
           	// The Fan is going into Bronze Tier for the first time. 
           	if ( rewardTierFanWillBeAssigned.Name == 'Bronze' )
           	{
           		CSRA_EarningTransactionServiceHelper ets = new CSRA_EarningTransactionServiceHelper();
            		
           		ets.addEarningTransactionForTier( 'Bronze Challenger' );
            		
           		pointsNeededForNextHigherTier = PointsNeededForThisTier( 'Silver', rewardTiersList );
           	}	
           		           
           	// The Fan is going into Silver Tier for the first time. 
           	if ( rewardTierFanWillBeAssigned.Name == 'Silver' )
           	{
           		//addEarningTransactionForTier( 'Silver Warrior' );
           		CSRA_EarningTransactionServiceHelper ets = new CSRA_EarningTransactionServiceHelper();
            		
           		ets.addEarningTransactionForTier( 'Silver Warrior' );
            		
           		pointsNeededForNextHigherTier = PointsNeededForThisTier( 'Gold', rewardTiersList );            		
           	}
            	
           	// The Fan is going into Gold Tier for the first time. 
           	if ( rewardTierFanWillBeAssigned.Name == 'Gold' )
           	{
           		//addEarningTransactionForTier( 'Gold Victor' );
           		CSRA_EarningTransactionServiceHelper ets = new CSRA_EarningTransactionServiceHelper();
            		
           		ets.addEarningTransactionForTier( 'Gold Victor' );            		
           	}   
        }
               
	    // requering because, Fan may have moved to highter Tier and Points and Tier values have changed.
	    List<Contact> contacts1 = [	Select Id
	       								,TotalPoints__c
	       								,RemainingPoints__c
	       								,PointsUntilNextTier__c
	       								,RewardTier__c 
	       							From Contact 
	       							where Id =: contactId];
	        
	    contacts1.get(0).RewardTier__c = rewardTierFanWillBeAssigned.Id; 
	        
	    // pointsNeededForNextHigherTier = 0 means point addition did not already push the fan into next higher tier,
	    // so just assign next level tier points.
	        
	    if ( pointsNeededForNextHigherTier == 0 )
	       	pointsNeededForNextHigherTier = pointsUntilNextTier;   
	        
	    // If the Fan is already at Gold, no more points needed for next tier.        
	    if(rewardTierFanWillBeAssigned.Name == 'Gold')
	       	contacts1.get(0).PointsUntilNextTier__c = 0; 
		else        	
	        contacts1.get(0).PointsUntilNextTier__c = pointsNeededForNextHigherTier - contacts1.get(0).TotalPoints__c; 
	    	
	    try {	              
	    	update contacts1.get(0);
	    }
	    catch(CSRA_RewardTierServiceHelperException e){	            
	        	System.debug( 'Reward Tier Service Helper : setFanTierAndGivePoints ' + ContactId );
	           	System.debug( 'Message ' + e.getMessage() );           	
        }
	    return contacts1.get(0);
    }
    
    private decimal PointsNeededForThisTier( String tierName, List<CSRA_RewardTier> rewardTiersList )
    { 
  		           
	    for( CSRA_RewardTier rwdTier : rewardTiersList )				
	    {		            
		   	if ( rwdTier.name == tierName )
		   	{
		   		return 	rwdTier.thresholdPoints;
		  	}	
		}  
  
  		return 0;  
    }	
}