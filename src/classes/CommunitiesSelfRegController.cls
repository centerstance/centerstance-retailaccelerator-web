/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
public with sharing class CommunitiesSelfRegController {

    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public String startURL {get; set;}
    
    private CommunitySettings__c communitySettings;
    
    public CommunitiesSelfRegController() {
    
    	//initialize community settings
    	communitySettings = CommunitySettings__c.getInstance();
    	
    	//get the start url
    	startURL = System.currentPageReference().getParameters().get('startURL');
    	System.debug('Start URL: ' + startURL);
    }
    
    private boolean isValidPassword() {
        return password == confirmPassword;
    }

    public PageReference registerUser() {
    	
    	//validate 
    	Boolean isValid = true;
    	
    	if(isEmptyString(firstName, true)){
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'First Name is required.'));
			isValid = false;
    	}
    	
    	if(isEmptyString(lastName, true)){
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Last Name is required.'));
			isValid = false;
    	}
    	
    	if(isEmptyString(communityNickname, true)){
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Nickname is required.'));
			isValid = false;
    	}
    	
    	if(isEmptyString(email, true)){
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Email is required.'));
			isValid = false;
    	}
    	
    	if(!isValid){
    		return null;
    	}
    
        // it's okay if password is null - we'll send the user a random password in that case
        if (!isValidPassword()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            return null;
        }    

		//get data from custom settings
        String profileId = (communitySettings.FanProfileId__c == null) ? null : communitySettings.FanProfileId__c; 
        String roleEnum = null; 
        String accountId = (communitySettings.FanAccountId__c == null) ? null : communitySettings.FanAccountId__c; 
        String userName = email;
        
      	//set user attributes
        User u = new User();
        u.Username = userName;
        u.Email = email;
        u.FirstName = firstName;
        u.LastName = lastName;
        u.CommunityNickname = communityNickname;
		u.ProfileId = profileId;
        
        try {
        	
        	//create the portal user
        	String userId = Site.createPortalUser(u, accountId, password);
        	
        	//if we have a valid user
        	if (userId != null) {
        		
        		//...and we have a valid password 
	            if (password != null && password.length() > 1) {
	            	//login to the site
	                return Site.login(userName, password, startURL); 
	            }
	            else {
	            	
	            	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Password.');
            		ApexPages.addMessage(msg);
            		return null;
	            	
	            	/*
	                PageReference page = System.Page.CommunitiesSelfRegConfirm;
	                page.setRedirect(true);
	                return page;
	                */
	            }
        	}
        	
        } catch(Exception e){
        	
        	System.debug('ERROR: ' + e.getMessage());
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to Sign Up. Please contact system administrator.');
    		ApexPages.addMessage(msg);
    		return null;
        	
        }
        
        return null;
    }
    
    
    //need to move this to String Utils
    private Boolean isEmptyString(String input, boolean trim) {
        if (input == null) {
            return true;
        }
        
        if (trim) {
            input = input.trim();
        }
        return input.equalsIgnoreCase('');
    } 
}