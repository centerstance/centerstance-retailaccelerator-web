// This is the base exception class that all custom exception should extend
// this allows us to catch all custom exceptions with a generic "catch (CustomException ce)"
public with sharing virtual class CS_CustomException extends Exception {
	public boolean isCustomException {public get{ return true;}}
}