public without sharing class CSRA_EarningServiceHelper {
    
    //custom exception
    public class EarningServiceHelperException extends CS_CustomException {} 
    
    //system logging debug level 
    private System.LoggingLevel debugLevel = LoggingLevel.INFO;
    
    //method to get all active earning actions
    public List<CSRA_EarningAction> getEarningActions () {      
        
        //initialize list
        List<CSRA_EarningAction> earningActions = new List<CSRA_EarningAction>();
        
        //get the list of active earning actions from DB
        List<EarningAction__c> listEarningActions = [	Select Id
	                                                        ,Name__c
	                                                        ,Description__c
	                                                        ,Points__c
	                                                        ,Order__c
	                                                        ,IsActive__c
	                                                        ,IconURL__c
	                                                        ,Frequency__c
	                                                        ,ExpirationDate__c
	                                                        ,EffectiveDate__c
	                                                        ,DisplayData__c
	                                                        ,DisplayData__r.Name
	                                                        ,DisplayData__r.LabelText__c
	                                                        ,DisplayData__r.AboveLabelText__c
	                                                        ,DisplayData__r.BelowLabelText__c
	                                                        ,DisplayData__r.PrimaryText__c
	                                                        ,DisplayData__r.HelpText__c
	                                                        ,DisplayData__r.NotificationMessage__c
	                                                        ,DisplayData__r.ButtonText__c
	                                                        ,DisplayData__r.BlueColorCode__c
	                                                        ,DisplayData__r.GreenColorCode__c
	                                                        ,DisplayData__r.RedColorCode__c
	                                                        ,DisplayData__r.HexColorCode__c
	                                                    From EarningAction__c 
	                                                    Where IsActive__c = true 
	                                                    And (EffectiveDate__c <= Today) And (ExpirationDate__c >= Today) 
	                                                    order by Order__c
                                                	];
        //if we get earning actions from DB
        if(!listEarningActions.IsEmpty()){
            
            //get the unique earning action ids 
            Map<Id, EarningAction__c> earningActionMap = (new Map<Id, EarningAction__c>(listEarningActions));
            Set<Id> earningActionIds = earningActionMap.keySet();
            
            system.debug( 'earningActionMap size ' + earningActionMap.size() + ' ' + earningActionIds);         
            
            // Build the Map for attachment and its Body, so we can get it when looping through the list of Earning Actions
            Map<Id,Attachment> mapEarnActionIdByIcon = new Map<Id,Attachment>();
            
            for(Attachment icon:[ Select Id, ParentId, Name, Body 
                                  From Attachment 
                                  Where ParentId IN :earningActionIds 
                                  and isDeleted = false 
                                ])
            {
                
                //if the map does not have the parent id key
                if(!mapEarnActionIdByIcon.containsKey(icon.ParentId)) {
                    
                    mapEarnActionIdByIcon.put(icon.ParentId, icon);
                    system.debug(debugLevel,  'Parent ' + icon.ParentId + ' icon ' + icon.name + ' Icon id ' +  icon.id);
                    
                } else {
                    
                    continue;
                }
            }
            
            //iterate earning actions and set attributes                    
            for ( EarningAction__c ea : listEarningActions){
                
                CSRA_EarningAction earningAction = new CSRA_EarningAction();
                CSRA_DisplayData displayData = new CSRA_DisplayData();
                        
                earningAction.id                = ea.id;    
                earningAction.name              = ea.Name__c;
                earningAction.description       = ea.Description__c;
                earningAction.effectiveDate     = ea.EffectiveDate__c;  
                earningAction.expirationDate    = ea.ExpirationDate__c;
                earningAction.points            = ea.Points__c.intValue();
                earningAction.displayOrder      = ea.Order__c.format();
                earningAction.iconUrl           = ea.IconURL__c;
                earningAction.frequency         = ea.Frequency__c;
                
                //set corresponding display data
                displayData.labelText           = ea.DisplayData__r.LabelText__c;
                displayData.aboveLabelText      = ea.DisplayData__r.AboveLabelText__c;
                displayData.belowLabelText      = ea.DisplayData__r.BelowLabelText__c;
                displayData.primaryText         = ea.DisplayData__r.PrimaryText__c;
                displayData.helpText            = ea.DisplayData__r.HelpText__c;
                displayData.buttonText          = ea.DisplayData__r.ButtonText__c;
                displayData.notificationMessage = ea.DisplayData__r.NotificationMessage__c;
                displayData.hexColorCode        = ea.DisplayData__r.HexColorCode__c;
                displayData.redColorCode        = ea.DisplayData__r.RedColorCode__c.format();               
                displayData.greenColorCode      = ea.DisplayData__r.GreenColorCode__c.format(); 
                displayData.blueColorCode       = ea.DisplayData__r.BlueColorCode__c.format();
                earningAction.displayData       = displayData;  
                
                // If there is a corresponding attachment to the Earning Action, then Get it from the Map.
                if (mapEarnActionIdByIcon.containsKey(ea.id) ) {
                    
                    earningAction.iconBinaryData  = EncodingUtil.base64Encode( mapEarnActionIdByIcon.get(ea.id).Body);
                }
                else {
                    
                    earningAction.iconBinaryData = '';
                    system.debug( debugLevel, 'mapEarnActionIdByIcon.get(earnAction.id) <> null ' + ea.id);
                }
                    
                //add to list   
                earningActions.add(earningAction);          
            }
        }
        else {
            
            throw new EarningServiceHelperException('Unable to get earning actions. Please contact system administrator.');
            system.debug (debugLevel,  'No Earning Action records found!' );                    
        }
                
        
        return earningActions;  
    }
    
    
    public CSRA_EarningAction getEarningActionById (Id earningActionId){
 		
 		//initialize 
 		CSRA_EarningAction earningAction = new CSRA_EarningAction();
 		
 		//validate input
 		if(earningActionId == null){
 			throw new CS_MissingParameterException('Missing parameter - earning action Id: ' + earningActionId);
 		}
 		
 		//build the criteria
 		String criteria = 'Id = ' + '\''  + earningActionId + '\'';
 		
 		//get earning action details
 		earningAction = getEarningAction(criteria);
 		
 		//return to caller
 		return earningAction;
 		
 	}
 	
 	public CSRA_EarningAction getEarningActionByName (string earningActionName){
 		
 		//initialize 
 		CSRA_EarningAction earningAction = new CSRA_EarningAction();
 		
 		//validate input
 		if(earningActionName == null || earningActionName == ''){
 			throw new CS_MissingParameterException('Missing parameter - earning action name: ' + earningActionName);
 		}
 		
 		//build the criteria
 		String criteria = 'Name__c = ' + '\''  + earningActionName + '\'' ;
 		
 		//get earning action details
 		earningAction = getEarningAction(criteria);
 		
 		return earningAction;
 	}
 	
 	private CSRA_EarningAction getEarningAction(String criteria) {
 		
 		//initialize 
 		CSRA_EarningAction earningAction = new CSRA_EarningAction();
 		
 		//validate input
 		if(criteria == null || criteria == ''){
 			throw new CS_MissingParameterException('Missing parameter - criteria: ' + criteria);
 		}
 		
 		//build the query string
 		String queryString = 'SELECT Id, Description__c, Points__c, Order__c, IsActive__c, IconURL__c, Frequency__c, ExpirationDate__c, EffectiveDate__c, Name__c, '
							+ 'DisplayData__c, DisplayData__r.Name, DisplayData__r.LabelText__c, DisplayData__r.AboveLabelText__c, DisplayData__r.BelowLabelText__c, '
 							+ 'DisplayData__r.PrimaryText__c, DisplayData__r.HelpText__c, DisplayData__r.NotificationMessage__c, DisplayData__r.ButtonText__c, '
 							+ 'DisplayData__r.BlueColorCode__c, DisplayData__r.GreenColorCode__c, DisplayData__r.RedColorCode__c, DisplayData__r.HexColorCode__c '
							+ 'FROM EarningAction__c  Where '
							+ criteria;
							
		system.debug( 'queryString ' + queryString );
		
		//get the earning action data from DB					
		List<EarningAction__c> earningActions = Database.query(queryString);
		
		//if we data, cretae and set earning action attributes
		if(!earningActions.isEmpty()) {
			
			EarningAction__c ea = earningActions.get(0);
			earningAction.id 				= ea.id;	
           	earningAction.name 				= ea.Name__c;
           	earningAction.description		= ea.Description__c;
           	earningAction.effectiveDate 	= ea.EffectiveDate__c;	
           	earningAction.expirationDate 	= ea.ExpirationDate__c;
           	earningAction.points 			= ea.Points__c.intValue();
           	earningAction.displayOrder		= ea.Order__c.format();
           	earningAction.iconUrl 			= ea.IconURL__c;
           	earningAction.frequency 		= ea.Frequency__c;
           	
           	//set corresponding display data
           	CSRA_DisplayData displayData	= new CSRA_DisplayData();
           	displayData.labelText 			= ea.DisplayData__r.LabelText__c;
           	displayData.aboveLabelText 		= ea.DisplayData__r.AboveLabelText__c;
           	displayData.belowLabelText 		= ea.DisplayData__r.BelowLabelText__c;
           	displayData.primaryText 		= ea.DisplayData__r.PrimaryText__c;
           	displayData.helpText 			= ea.DisplayData__r.HelpText__c;
           	displayData.buttonText 			= ea.DisplayData__r.ButtonText__c;
           	displayData.notificationMessage = ea.DisplayData__r.NotificationMessage__c;
           	displayData.hexColorCode 		= ea.DisplayData__r.HexColorCode__c;
           	displayData.redColorCode 		= ea.DisplayData__r.RedColorCode__c.format();	           	
           	displayData.greenColorCode 		= ea.DisplayData__r.GreenColorCode__c.format();	
           	displayData.blueColorCode 		= ea.DisplayData__r.BlueColorCode__c.format();
           	earningAction.displayData 		= displayData;	
		
			
		} else {
			
			throw new EarningServiceHelperException('Unable to get earning action. Please contact system administrator.');
		}
 		
 		//return to caller
 		return earningAction;
 	}

}